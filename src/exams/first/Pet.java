package exams.first;

/**
 * @author Chunliang Chen
 * @version 26-02-2022_14:38:17
 * @class CS201
 */
public class Pet {
    private String name;
    private int age;
    public Pet() {
        name="pet";
        age=28;
    }
    public Pet(String n,int a) {
        name=n;
        age=a;
    }
    public void  setName(String name){
        this.name=name;
    }
    public void setAge(int age){
        this.age=age;
    }
    public String getName(){
        return name;
    }
    public int getAge(){
        return age;
    }
    public boolean equals(Pet p){
        if(this.name.equals(getName())){
            return  false;
        } else  if(this.age != p.getAge()){
            return  false;
        }
       return  true;
    }
    public String toString(){
        return name+"-"+
                age;
    }
}
