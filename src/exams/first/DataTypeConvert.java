package exams.first;
import  java.util.Scanner;
/**
 * @author  Chunliang Chen
 * @version 26-02-2022_04:58:36
 * @class  CS201
 */
public class DataTypeConvert {
    public static  void main(String[] args){
       Scanner sc=new  Scanner(System.in);
        try{System.out.println("Please input an integer:");
       int a=sc.nextInt();
       char b= (char) ((char)a+65);
       System.out.print("Convert the result of " + a + " plus 65 to a character is " + b +".");
       sc.close();
    }catch(Exception e){
        System.out.println("ERROR");
    }
        sc.close();
    }
}
