package exams.first;

import java.util.Scanner;

/**
 * @author Chunliang Chen
 * @version 26-02-2022_06:11:06
 * @class CS201
 */
public class ArrayTest {
    public static  void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] arr = new String[5];
        for (int i = 0; i < 5; i++) {
            int j = i + 1;
            System.out.println("Please input the number of order " + j + ":");
            arr[i] = sc.next();
        }
        String[] arr1=new String[5];
        int index=0;
        boolean x=false;
        arr1[index++]=arr[0];
        for (int k = 0; k < 5; k++) {
            for (int l = 0; l<index; l++) {
                if((arr[k]).equals(arr1[l])) {
                    x=true;
                    break;
                }
            }
            if(x==false){
                arr1[index++]=arr[k];
            }
            x=false;
        }
        arr =new String[index];
        System.arraycopy(arr1, 0, arr, 0, index);
        for(String element:arr){
            System.out.print(element+ "\t");
        }
        System.out.println();
        arr = null;
    }
}
