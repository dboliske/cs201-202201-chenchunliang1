package exams.first;

import java.util.Scanner;
/**
 * @author Chunliang Chen
 * @version 26-02-2022_05:06:01
 * @class CS201
 */
public class Selection {
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        try {System.out.println("Please input an integer :");
        int a= sc.nextInt();
        if((a % 2 ==0) && (a % 3!=0)){
            System.out.println("foo");
        }else if((a % 2 !=0) && (a % 3==0)){
            System.out.println("bar");
        }else if((a % 2 ==0) && (a % 3==0)){
            System.out.println("foobar");
        }
       }catch (Exception e) {
            System.out.println("ERROR");
            sc.close();
        }
        sc.close();
    }
}
