package exams.first;

import java.util.Scanner;

/**
 * @author Chunliang Chen
 * @version 26-02-2022_05:23:55
 * @class CS201
 */
public class Repetition {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try {
            System.out.println("please input  the side length of a square:");
            int a = sc.nextInt();
            System.out.println("Print square ");
            for (int i = 0; i <= a; i++) {
                for (int j = 0; j < i; j++) {
                    System.out.print("  ");
                }
                for (int k = a; k > i; k--) {
                    System.out.print("* ");
                }
                System.out.println();
            }
        } catch (Exception e) {
            System.out.println("ERROR");
            sc.close();
        }
    }
}