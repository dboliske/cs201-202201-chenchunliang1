package exams.second;

/**
 * @author Chunliang Chen
 * @version 30-04-2022_12:14:00
 * @class CS201
 */
public class StringArraySort {
    public static void main(String[] args) {
        String[] input=new String[] {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
        String[] output=arraySort(input);
        for (String out:output) {
            System.out.println(out);
        }
    }

    public static String[] arraySort(String[] input){
        for (int i=0;i<input.length-1;i++){
            for (int j=0;j<input.length-i-1;j++) {
                if(isBiggerThan(input[j],input[j+1])){
                    String temp=input[j];
                    input[j]=input[j+1];
                    input[j+1]=temp;
                }
            }
        }
        return input;
    }

    private static boolean isBiggerThan(String first, String second){
        if(first==null||second==null||"".equals(first) || "".equals(second)){
            System.out.println("The string cannot be empty!");
            return false;
        }
        char[] arrayfirst=first.toCharArray();
        char[] arraysecond=second.toCharArray();
        int minSize = Math.min(arrayfirst.length, arraysecond.length);
        for (int i=0;i<minSize;i++) {
            if((int)arrayfirst[i]>(int)arraysecond[i]){
                return true;
            }else if((int)arrayfirst[i] < (int)arraysecond[i]){
                return false;
            }
        }
        if(arrayfirst.length>arraysecond.length){
            return true;
        }else {
            return false;
        }
    }
}
