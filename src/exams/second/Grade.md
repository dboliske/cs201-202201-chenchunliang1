# Final Exam

## Total

79/100

## Break Down

1. Inheritance/Polymorphism:    17/20
    - Superclass:               5/5
    - Subclass:                 5/5
    - Variables:                3/5
    - Methods:                  4/5
2. Abstract Classes:            17/20
    - Superclass:               3/5
    - Subclasses:               5/5
    - Variables:                4/5
    - Methods:                  5/5
3. ArrayLists:                  15/20
    - Compiles:                 5/5
    - ArrayList:                5/5
    - Exits:                    0/5
    - Results:                  5/5
4. Sorting Algorithms:          15/20
    - Compiles:                 5/5
    - Selection Sort:           5/10
    - Results:                  5/5
5. Searching Algorithms:        15/20
    - Compiles:                 5/5
    - Jump Search:              5/10
    - Results:                  5/5

## Comments

1. Attribute building and roomNumber should be protected. -2. 
   The setSeats method didn't check the validation of seats. -1.
2. Methods area and perimeter in Polygon should be abstract. -2. 
   The permission modifier of attribute ‘name’ should be protected -1.
3. The program didn't exist as required. -5.
4. The program did't implement selection sort algorithm. -5
5. The program did't implement jump search algorithm recursively. -5
