package exams.second;

/**
 * @author Chunliang Chen
 * @version 30-04-2022_10:29:43
 * @class CS201
 */
public class Circle extends Polygon{
    private  double raduius;
    final double PI=3.14;
    public  Circle(){
        super();
    }

    public void setRaduius(double raduius) {
        this.raduius = raduius;
    }

    public double getRaduius() {
        return raduius;
    }
    @Override
    public double area(){
        return PI*raduius*raduius;
    }
    @Override
    public double perimeter(){
        return 2*PI*raduius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "raduius=" + raduius +
                ", PI=" + PI +
                '}';
    }
}
