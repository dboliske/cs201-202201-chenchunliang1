package exams.second;

/**
 * @author Chunliang Chen
 * @version 30-04-2022_09:45:11
 * @class CS201
 */
public abstract class Polygon {
    private  String name;
    public Polygon(){
        super();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Polygon{" +
                "name='" + name +
                '}';
    }
    public double area() {
        return 0;
    }

    public double perimeter() {
        return 0;
    }

}
