package exams.second;

/**
 * @author Chunliang Chen
 * @version 30-04-2022_07:18:31
 * @class CS201
 */
public class Classroom {
    private  String building;
    private  String roomNumber;
    private  int seats;
    public Classroom(){
        super();
    };
    public void setBuilding(String building) {
        this.building = building;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }
    public String getBuilding() {
        return building;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public int getSeats() {
        return seats;
    }

    @Override
    public String toString() {
        return "Classroom{" +
                "building='" + building + '\'' +
                ", roomNumber='" + roomNumber + '\'' +
                ", seats=" + seats +
                '}';
    }
}
