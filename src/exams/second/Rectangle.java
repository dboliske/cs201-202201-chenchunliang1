package exams.second;

/**
 * @author Chunliang Chen
 * @version 30-04-2022_10:08:48
 * @class CS201
 */
public class Rectangle extends Polygon {

    private  double width;
    private  double height;
    public Rectangle(){
        super();
    }
    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }
    @Override
    public double area(){
        return width*height;
    }
    public double perimeter(){
        return 2*(width+height);
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }
}
