package exams.second;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Chunliang Chen
 * @version 30-04-2022_11:06:24
 * @class CS201
 */
public class ArrayListsTest {
    public static void main(String[] args) {
        Scanner num=new Scanner(System.in);
        System.out.println("Please enter the number and space it out!");
        String str=num.nextLine();
        String[] srr=str.split(" ");
        ArrayList<Double> arr=new ArrayList<>();
        for (int i = 0; i < srr.length; i++) {
            arr.add(Double.valueOf(srr[i]));
        }
        System.out.println(arr);
        num.close();
        double max = arr.get(0);
        for(int i = 0; i < arr.size(); i++) {
            if(max < arr.get(i)) {
                max = arr.get(i);
            }
        }
        System.out.println("The Maximum value is："+max);

        double min = arr.get(0);
        for(int i = 0; i < arr.size(); i++) {
            if(min > arr.get(i)) {
                min = arr.get(i);
            }
        }
        System.out.println("Then Minimum value is："+min);

    }
}
