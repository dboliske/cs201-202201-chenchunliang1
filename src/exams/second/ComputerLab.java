package exams.second;

import jdk.nashorn.api.scripting.ScriptObjectMirror;

/**
 * @author Chunliang Chen
 * @version 30-04-2022_07:26:14
 * @class CS201
 */
public class ComputerLab extends Classroom{
    private  boolean computers;
    public ComputerLab(){
        super();
    }
    public boolean hasComputers() {
        return computers;
    }

    public void setComputers(boolean computers) {
        this.computers = computers;
    }
    @Override
    public String toString() {
        return "Classroom{" +
                "building=" + getBuilding() +
                ", roomNumber='" + getRoomNumber() + '\'' +
                ", seats=" + getSeats() +
                ", computers='" + computers +
                '}';
    }
}
