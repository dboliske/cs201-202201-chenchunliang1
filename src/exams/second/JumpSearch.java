package exams.second;

import java.util.Scanner;

/**
 * @author Chunliang Chen
 * @version 30-04-2022_12:23:17
 * @class CS201
 */
public class JumpSearch {
    public static void main(String [ ] args)
    {
        double arr[] = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
        Scanner scan=new Scanner(System.in);
        System.out.println("Please enter the number you are looking for：");
        double x= scan.nextDouble();
        int index = jumpSearch(arr, x);
        System.out.println("\nNumber " + x +
                " is at index " + index);
    }

    public static int jumpSearch(double[] arr, double x)
    {
        int n = arr.length;
        int step = (int)Math.floor(Math.sqrt(n));
        int index = 0;
        while (arr[Math.min(step, n)-1] < x)
        {
            index = step;
            step += (int)Math.floor(Math.sqrt(n));
            if (index >= n)
                return -1;
        }
        while (arr[index] < x)
        {
            index++;
            if (index == Math.min(step, n))
                return -1;
        }
        if (arr[index] == x)
            return index;
        return -1;
    }
}
