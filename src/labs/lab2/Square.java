package labs.lab2;

import java.util.Scanner;

/** 
* @author  Chunliang Chen
* @version 05-02-2022_11:03:21
* @class  CS201
*/
public class Square {

	public static void main(String[] args) {
	    Scanner a=new Scanner(System.in);
	    System.out.println("Please input the length of squre :");
	    int length=a.nextInt();
	    int i;
	    int j;
	    for (i=0;i<length;i++) {
	    	for (j=0;j<length;j++) {
	    		System.out.print("* ");
	    	}
	    	System.out.println();
	    }
	    a.close();
	}

}
