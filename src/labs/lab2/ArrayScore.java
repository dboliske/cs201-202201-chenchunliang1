package labs.lab2;

import java.util.Scanner;
/** 
* @author  Chunliang Chen
* @version 05-02-2022_13:10:00
* @class  CS201
*/
public class ArrayScore {

	public static void main(String[] args) {
        Scanner a = new Scanner(System.in);
        System.out.print("Please input the number of student:");
        int num=a.nextInt();
        double[] score=new double[num];
        double sum=0;
        for (int i=0;i<score.length;i++) {
        	System.out.print("Grade"+(i+1)+". Please input the grade :");
        	score[i]=a.nextDouble();
        	sum+=score[i];
        }
        double avg=sum/num;
        System.out.println("The average grade of students is "+avg+" .");
        System.out.print("\n"+"Do you want to continue(End of input -1)?");
        int j = a.nextInt();
        System.out.println(j);
        while (j != -1) {
               System.out.print("Please input next score:");
               sum+=a.nextDouble();
               num+=1;
               avg=sum/num;
               System.out.println("The average grade of students is "+avg+" .");
               System.out.print("\n"+"Do you want to continue(End of input -1)?");
               j = a.nextInt();
         }
        System.out.println("Done!"); 
        a.close();		   
     }
}