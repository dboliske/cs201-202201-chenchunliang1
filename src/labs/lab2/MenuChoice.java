package labs.lab2;

import java.util.Scanner;

/** 
* @author  Chunliang Chen
* @version 05-02-2022_19:34:12
* @class  CS201
*/
public class MenuChoice {

	public static void main(String[] args)  {
		  Scanner scanner = new Scanner(System.in);
	  while (true) {
		  System.out.println("Please choice your option:");
	      System.out.println("   Option 1. Say Hello");
	      System.out.println("   Option 2. Multiplication");
	      System.out.println("   Option 3. Multiplication");
	      System.out.println("   Option 4. Exit");
	      System.out.print("Your choice is :");
	      int i = scanner.nextInt();
	      System.out.println("-------------------------");
      switch(i) {
      case 1:
    	  System.out.println("Hello");
	      System.out.println("-------------------------");
    	  break;
      case 2:
    	  System.out.print("Please input  num1:");
    	  double num1 = scanner.nextDouble();
    	  System.out.print("Please input  num2:");
    	  double num2 = scanner.nextDouble();
    	  System.out.println("Num1 plus num2 is "+(num1+num2));
	      System.out.println("-------------------------");
    	  break; 
      case 3:
    	  System.out.print("Please input number a:");
    	  double a = scanner.nextDouble();
    	  System.out.print("Please input number b:");
    	  double b = scanner.nextDouble();
    	  System.out.println("A multiply b is "+(a*b));
	      System.out.println("-------------------------");
    	  break;   	  
      case 4:
    	  System.exit(0);  
      }  
         if (i==4){
          System.out.println("Option 4 exit the program.");
          break;
      }
	}
	scanner.close();
	}
}
