package labs.lab3;
/** 
* @author  Chunliang Chen
* @version 14-02-2022_19:57:42
* @class  CS201
*/
public class MinimumValue {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr=new int[]{72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 
				32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 
				108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421} ;
		int min=arr[0];
		for(int i=1;i<arr.length;i++){
			if(arr[i]<min){
				min=arr[i];
			}
		}
		System.out.println("the minimum value is:"+min);
	}

}
