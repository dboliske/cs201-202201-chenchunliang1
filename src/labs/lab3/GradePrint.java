package labs.lab3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/** 
* @author  Chunliang Chen
* @version 14-02-2022_18:31:11
* @class  CS201
*/
public class GradePrint {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String csvFile = "cs201-202201-chenchunliang1\\bin\\labs\\lab3\\grades.csv";
        String line = "";
        String cvsSplitBy = ",";
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            while ((line = br.readLine()) != null) {
                String[] grade = line.split(cvsSplitBy);
                System.out.println("Grade [students name = " + grade[0] + " , grade =" + grade[1] + "]");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

	}

}
