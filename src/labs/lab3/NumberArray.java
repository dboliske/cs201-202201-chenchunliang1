package labs.lab3;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/** 
* @author  Chunliang Chen
* @version 14-02-2022_18:57:25
* @class  CS201
*/
public class NumberArray
{
    private static List<Double> numList = new ArrayList<Double>();
    private static String ss;
    private static String newss;
    private static int m = 0;
    private static BufferedReader br;
    public static void main(String[] args)
    {
        System.out.println("Please input a number,until they enter \"Done\" to finish");
        try
        {
            br = new BufferedReader(new InputStreamReader(System.in));
            ss = br.readLine();
            newss = String.valueOf(ss);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        while(String.valueOf(newss) != "Done")
        {
            try
            {
                ss = br.readLine();
                newss = String.valueOf(ss);
                numList.add(m, Double.valueOf(newss));
                m++;
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
