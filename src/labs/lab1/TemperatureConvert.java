package labs.lab1;

import java.util.Scanner;

/** 
* @author  Chunliang Chen
* @version 05-02-2022_07:48:36
* @class  CS201
*/
public class TemperatureConvert {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	  String TemperatureTest;
      Scanner a=new Scanner(System.in);
      System.out.println("Please input a temperature in Fahrenheit:");
      float F1=a.nextFloat();
      float C1=(float) ((F1-32)/1.8);//Degrees Celsius = (Fahrenheit - 32��F) �� 1.8
	  if(C1<-10) {
		  TemperatureTest="low";
	  }
	    else if(C1>50)
	    {
			  TemperatureTest="high";
		  }
	    else 	    {
			  TemperatureTest="middle";
		  };
      System.out.println("Convert the Fahrenheit to Celsius  gives "+C1+"��C.");
      System.out.println("The temperature is "+TemperatureTest+". ");
      System.out.println("------------------------------------------");
      System.out.println("Please input a temperature in Celsius:");
      float C2=a.nextFloat();
      float F2=9*C2/5+32;    //F��9��C��5��32
	  if(C2<-10) {
		  TemperatureTest="low";
	  }
	    else if(C2>50)
	    {
			  TemperatureTest="high";
		  }
	    else 	    {
			  TemperatureTest="middle";
		  };
      System.out.println("Convert the Celsius to Fahrenheit  gives "+F2+"�H.");
      System.out.println("The temperature is "+TemperatureTest+". ");
      a.close();
	}

}
