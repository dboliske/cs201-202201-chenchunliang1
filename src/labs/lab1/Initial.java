package labs.lab1;

import java.util.Scanner;

/** 
* @author  Chunliang Chen
* @version 05-02-2022_07:39:06
* @class  CS201
*/
public class Initial {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner a=new Scanner(System.in);
        System.out.println("Please  input your first name:");
        String firstName=a.nextLine();
        System.out.println("Your first initial is "+ firstName.charAt(0)+".");
        a.close();
	}

}
