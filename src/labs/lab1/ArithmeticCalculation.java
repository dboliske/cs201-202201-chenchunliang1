package labs.lab1;
/** 
* @author  Chunliang Chen
* @version 05-02-2022_06:44:03
* @class  CS201
*/
/*
 * (age1-age2) * 2
 * */

import java.util.Scanner;

public class ArithmeticCalculation {
   public static  void main(String[] args) {
   Scanner a = new Scanner(System.in);
   System.out.println("Please input your father'age:");
   int age1=a.nextInt();
   System.out.println("Please input your age:");
   int age2=a.nextInt();
   int age=age1-age2;//Your age subtracted from your father's age
   System.out.println("Please input your birth year:");
   int birthYear=a.nextInt();
   int birthYear2=birthYear*2;//Your birth year multiplied by 2
   System.out.println("Your age subtracted from your father's age gives "+age+".");
   System.out.println("Your birth year multiplied by 2 gives "+birthYear2+".");
   System.out.print("Please input your height in inches:");
   float  height=a.nextFloat();
   float  heightCoventCms=(float) (height*2.54);//1 inch = 2.54 centimeters
   int    heightCoventInches=(int) height;
   int    heightCoventFeet=(int) (height/12);//1 foot = 12 inches
   System.out.println("Convert your height in inches to cms gives "+heightCoventCms+".");
   System.out.println("Convert your height in inches to feet  where inches is an integer gives "+heightCoventFeet+".");
   System.out.println("Convert your height in inches to inches where inches is an integer gives "+heightCoventInches+".");
   a.close();
   }
   
}
