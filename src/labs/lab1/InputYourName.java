package labs.lab1;

import java.util.Scanner;

/** 
* @author  Chunliang Chen
* @version 05-02-2022_06:37:43
* @class  CS201
*/
public class InputYourName {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
       Scanner a=new Scanner(System.in);
       System.out.print("please input your name:");
       String name=a.nextLine();
       System.out.println("Your name is "+name+" .");
       a.close();
	}

}
