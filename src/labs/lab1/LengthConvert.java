package labs.lab1;

import java.util.Scanner;

/** 
* @author  Chunliang Chen
* @version 05-02-2022_08:50:45
* @class  CS201
*/
public class LengthConvert {

	public static void main(String[] args)  {
		Scanner a=new Scanner(System.in); 
	    System.out.println("Please input the length in inches:");
	    double inchesLength=a.nextDouble();
	    double cmsLength=inchesLength*2.54;//1 inch = 2.54 centimeters
	    System.out.println("Convert the inches to centimeters gives "+cmsLength+ "cm.");
	    a.close();
	    String str  = "Scanner a=new Scanner(System.in);"+"\n"
	            +"System.out.println(\"Please input the length in inches:\");"+"\n"
	            +"double inchesLength=a.nextDouble();"+"\n"
	            +"double cmsLength=inchesLength*2.54;//1 inch = 2.54 centimeters"+"\n"
	            +"System.out.println(\"Convert the inches to centimeters gives \"+cmsLength+ \"cm.\");"+"\n"
	            +"a.close();";
	    int lines = 1; 
	    int pos = 0; 
	    while ((pos = str.indexOf("\n", pos) + 1) != 0) {
	    	lines++; 
	    	} 
				System.out.println("The rows of program is " +lines+".");
	 }
}
