# Lab 1

## Total

17/20

## Break Down

* Exercise 1    2/2
* Exercise 2    2/2
* Exercise 3    2/2
* Exercise 4
  * Program     2/2
  * Test Plan   0/1
* Exercise 5
  * Program     2/2
  * Test Plan   0/1
* Exercise 6
  * Program     2/2
  * Test Plan   0/1
* Documentation 5/5

## Comments

Code looks good, but no test results.
