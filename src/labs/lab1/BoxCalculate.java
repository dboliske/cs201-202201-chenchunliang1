package labs.lab1;

import java.util.Scanner;

/** 
* @author  Chunliang Chen
* @version 05-02-2022_08:32:00
* @class  CS201
*/
public class BoxCalculate {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
      Scanner a=new Scanner(System.in); 
      System.out.println("Please input the length in inches  of the box :");
      double length=a.nextDouble();
      System.out.println("Please input the width in inches of the box:");
      double width=a.nextDouble();
      System.out.println("Please input the depth in inches  of the box:");
      double depth=a.nextDouble();
      double surfaceArea=(length/12*width/12+width/12*depth/12+depth/12*length/12)*2; 
      //surfaceArea=(length*width+width*depth+depth*length)*2   
      //1 foot = 12 inches
      System.out.println("The amount of wood box is "+surfaceArea+" square foot.");
      a.close();
	}

}
