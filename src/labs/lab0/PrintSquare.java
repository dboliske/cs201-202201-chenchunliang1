package labs.lab0;
/** 
* @author  Chunliang Chen
* @version 2022-01-26 09:36:54
* @class  CS201
*/
import java.util.Scanner;
public class PrintSquare {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	    Scanner a = new Scanner(System.in);	
	    System.out.print("please input  the side length of a square:");
	    int length = a.nextInt();
		System.out.println("The side length of a square is "+ length + ".");
		printSolidSqure(length);
		a.close();
	}
	private static void printSolidSqure(int length){
		Scanner a = new Scanner(System.in);	
		System.out.print("please input the type of a square is:");
		String type = a.nextLine();
		System.out.println("The type of a square is '"+ type + "'.");
		int i;
		int j;
		for (i=0;i<length;i++) {
			for (j=0;j<length;j++) {
				System.out.print(type + " ");
			}
			System.out.println();
		}
		a.close();
	}

}
