package labs.lab0;

public class TryVariables {

	public static void main(String[] args) {
		// This example shows how to declare and initialize variables

		int integer = 100;
		long large = 42561230L;
		int small = 25;//data type error
		byte tiny = 19;

		float f = .0925F;//in the  end without semicolon
	    double decimal = 0.725;
		double largeDouble = +6.022E23; // This is in scientific notation

		char character = 'A';
		boolean t = true;

		System.out.println("integer is " + integer);
		System.out.println("large is " + large);
		System.out.println("small is " + small); //capitalize first letter 
		System.out.println("tiny is " + tiny);//without such  variable,guess tiny
		System.out.println("f is " + f);
		System.out.println("decimal is " + decimal);//comma
		System.out.println("largeDouble is " + largeDouble);
		System.out.println("character is " + character);//plus
		System.out.println("t is " + t);

	}

}