package labs.lab5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Chunliang Chen
 * @version 08-03-2022_08:04:40
 * @class CS201
 */
public class CTAStopApp {
    public static void main(String[] args) {
        CTAStopApp app = new CTAStopApp();

        CTAStation[] stations = app.readFile("CTAStops.csv");

        app.menu(stations);
    }

    CTAStation[] readFile(String filename) {
        ArrayList<CTAStation> stationList = new ArrayList<>();
        try {
            FileReader fr = new FileReader(filename);
            BufferedReader bf = new BufferedReader(fr);
            String str;
            bf.readLine();
            while ((str = bf.readLine()) != null) {

                str = str.trim();
                String[] arr = str.split(",");
                CTAStation st = new CTAStation(arr[0],
                        Double.parseDouble(arr[1]),
                        Double.parseDouble(arr[2]),
                        arr[3],
                        Boolean.parseBoolean(arr[4]),
                        Boolean.parseBoolean(arr[5])
                );
                stationList.add(st);

            }
            bf.close();
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        int length = stationList.size();
        CTAStation[] res = new CTAStation[length];
        for (int i = 0; i < length; i++) {
            res[i] = stationList.get(i);
        }

        return res;
    }

    void printOptions() {
        System.out.println("============================= menu ==============================");
        System.out.println("please choose the following options(just type the number): ");
        System.out.println("1) Display Station Names");
        System.out.println("2) Display Stations with/without Wheelchair access");
        System.out.println("3) Display Nearest Station ");
        System.out.println("4) Exit");
        System.out.println("=================================================================");
    }

    void menu(CTAStation[] stations) {
        printOptions();

        Scanner input = new Scanner(System.in);
        int choice = input.nextInt();
        while (choice != 4) {
            switch (choice) {
                case 1:
                    displayStationNames(stations);
                    break;
                case 2:
                    displayByWheelChair(input, stations);
                    break;
                case 3:
                    displayNearest(input, stations);
                    break;
            }
            printOptions();
            choice = input.nextInt();
        }

        System.out.println("The program stops running now!");

    }

    void displayStationNames(CTAStation[] stations) {
        System.out.println("The stations' name are as follows:");
        for (CTAStation st : stations) {
            System.out.println(st.getName());
        }
    }

    void displayByWheelChair(Scanner input, CTAStation[] stations) {
        System.out.println("Display Stations with/without Wheelchair access (type 'y' for YES, type 'n' for NO)");
        String choice = input.next();
        if ("y".equals(choice) || "Y".equals(choice)) {
            boolean flag = false;
            for (CTAStation st : stations) {
                if (st.hasWheelchair()) {
                    flag = true;
                    System.out.println(st);
                }
            }

            if (!flag) {
                System.out.println("no stations are found!");
            }
        } else if ("n".equals(choice) || "N".equals(choice)) {
            boolean flag = false;
            for (CTAStation st : stations) {
                if (!st.hasWheelchair()) {
                    flag = true;
                    System.out.println(st);
                }
            }

            if (!flag) {
                System.out.println("no stations are found!");
            }
        } else {
            System.out.println("The input cannot be processed!");
        }
    }

    void displayNearest(Scanner input, CTAStation[] stations) {
        System.out.println("Please input the latitude of a place:");
        double lat = input.nextDouble();
        System.out.println("Please input the longitude of a place:");
        double lng = input.nextDouble();
        double minDistance = Double.MAX_VALUE;

        int idx = -1;
        for (int i = 0; i < stations.length; i++) {
            CTAStation st = stations[i];
            double distance = st.calcDistance(lat, lng);
            if (distance < minDistance) {
                minDistance = distance;
                idx = i;
            }
        }

        System.out.println("the nearest station is " + stations[idx]);

    }
}
