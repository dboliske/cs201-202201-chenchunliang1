# Lab 5

## Total

17/20

## Break Down

CTAStation

- Variables:                    1/2
- Constructors:                 1/1
- Accessors:                    2/2
- Mutators:                     2/2
- toString:                     1/1
- equals:                       2/2

CTAStopApp

- Reads in data:                0/2
- Loops to display menu:        2/2
- Displays station names:       1/1
- Displays stations by access:  2/2
- Displays nearest station:     2/2
- Exits                         1/1

## Comments

- In CTAStation didn't check the validation of lat and lng -1;
- In CTAStopApp, the function of reading in data is wrong because of the wrong file path. -2. 
