package labs.lab5;
import java.util.Objects;

/**
 * @author Chunliang Chen
 * @version 08-03-2022_03:25:44
 * @class CS201
 */
public class CTAStation extends GeoLocation {
    private String name;
    private String location;
    private boolean wheelchair;
    private boolean open;

    public CTAStation() {

    }

    public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open) {
        super(lat, lng);
        this.name = name;
        this.location = location;
        this.wheelchair = wheelchair;
        this.open = open;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setWheelchair(boolean wheelchair) {
        this.wheelchair = wheelchair;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public String getName() {
        return this.name;
    }

    public String getLocation() {
        return this.location;
    }

    public boolean hasWheelchair() {
        return this.wheelchair;
    }


    public boolean isOpen() {
        return this.open;
    }

    @Override
    public String toString() {
        return "CTAStation{" +
                "name='" + this.name + '\'' +
                ", location='" + this.location + '\'' +
                ", wheelchair=" + this.wheelchair +
                ", open=" + this.open +
                ", " + super.toString() +
                "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CTAStation that = (CTAStation) o;
        return wheelchair == that.wheelchair && open == that.open && name.equals(that.name) && location.equals(that.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, location, wheelchair, open);
    }
}
