package labs.lab6;

/**
 * @author Chunliang Chen
 * @version 21-03-2022_11:53:06
 * @class CS201
 */
public class Customer {
    private String cid;
    //customer id
    private String name;
    //customer name
    private String age;
    //customer age
    private String address;
    //customer address
    public Customer() {
    }
    public Customer(String cid, String name, String age, String address) {
        this.cid = cid;
        this.name = name;
        this.age = age;
        this.address = address;
    }
    public String getcid() {
        return cid;
    }
    public void setcid(String cid) {
        this.cid = cid;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAge() {
        return age;
    }
    public void setAge(String age) {
        this.age = age;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
}
