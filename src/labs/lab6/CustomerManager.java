package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Chunliang Chen
 * @version 21-03-2022_11:43:37
 * @class CS201
 */
public class CustomerManager {
    /*
      1. Add customer to queue
      2. Help customer
      3. Exit
    */
    public static void main(String[] args) {
        ArrayList<Customer> array = new ArrayList<Customer>();

        while (true) {
            // 1. Main Interface
            System.out.println("---------Welcome to Customer Management System---------");
            System.out.println("1.Add customer to queue ");
            System.out.println("2.Help customer");
            System.out.println("3.Exit ");

            // 2. Data entry
            Scanner scanner = new Scanner(System.in);
            int line = scanner.nextInt();

            //3. switch
            switch (line) {
                case 1:
                    //System.out.println("Add customer");
                    addCustomer(array);
                    break;
                case 2:
                    //System.out.println("remove customer");
                    deleteCustomer(array);
                    break;
                case 3:
                    //System.out.println("Exit");
                    System.out.println("Exit");
                    System.exit(0);
            }
        }
    }

    public static void addCustomer(ArrayList<Customer> array) {

        Scanner sc = new Scanner(System.in);
        String cid;
        while (true) {
            System.out.println("Please input customer's name：");
            cid = sc.nextLine();

            boolean flag = isUsed(array, cid);
            if (flag) {
                System.out.println("The customer you entered already exists！");
            } else {
                break;
            }
        }

        System.out.println("Please input customer's name：");
        String name = sc.nextLine();
        System.out.println("Please input customer's age：");
        String age = sc.nextLine();
        System.out.println("Please input customer's address：");
        String address = sc.nextLine();

        Customer s = new Customer();
        s.setcid(cid);
        s.setName(name);
        s.setAge(age);
        s.setAddress(address);

        //save
        array.add(s);

        //Add Success Alert
        System.out.println("Add Customer Success");
    }

    //Define a method to determine if a client number is occupied
    public static boolean isUsed(ArrayList<Customer> array, String cid) {
        //judgement
        boolean flag = false;
        for (int i = 0; i < array.size(); i++) {
            Customer s = array.get(i);
            if (s.getcid().equals(cid)) {
                flag = true;
                break;
            }
        }
        return flag;
    }
    //Define a method to show customer
    public static void findAllCustomer(ArrayList<Customer> array) {
        if (array.size() == 0) {
            System.out.println("No information, please add information first and then query");
            return;
        }

        System.out.println("CustomerCid\t\t\tCustomerName\t\tCustomerAge\t\tCustomerAddress");

        for (int i = 0; i < array.size(); i++) {
            Customer s = array.get(i);
            System.out.println(s.getcid() + "\t\t" + s.getName() + "\t\t" + s.getAge() + "岁\t\t" + s.getAddress());
        }
    }

    //Define a method to Remove Customer
    public static void deleteCustomer(ArrayList<Customer> array) {

        // Keyboard entry of the customer's school number to be deleted, displaying a prompt message

        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter the customer number of the customer to be deleted");
        String cid = sc.nextLine();

        int index = -1;

        // Iterate through the collection to remove the corresponding customer object from the collection
        for (int i = 0; i < array.size(); i++) {
            Customer s = array.get(i);
            if (s.getcid().equals(cid)) {
                index = i;
                break;
            }
        }

        if (index == -1) {
            System.out.println("The customer number does not exist, please re-enter");
        } else {
            array.remove(index);
            System.out.println("Delete Customer Success!");
        }
    }
    //Define a method to modify customer information
    public static void updataCustomer(ArrayList<Customer> array) {
        // Keyboard entry of the customer number to be modified, displaying a prompt message

        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter the customer number of the customer to be modified");
        String cid = sc.nextLine();
        int index = -1;
        for (int i = 0; i < array.size(); i++) {
            Customer s = array.get(i);
            if (s.getcid().equals(cid)) {
                index = i;
                break;
            }
        }

        if (index == -1) {
            System.out.println("The customer number does not exist, please re-enter");
        } else {
            for (int j = 0; j < array.size(); j++) {
                Customer Customer = array.get(j);
                if (Customer.getcid().equals(cid)) {
                    // Keyboard entry of the customer information to be modified
                    System.out.println("Please enter the new name of the customer");
                    String name = sc.nextLine();
                    System.out.println("Please enter the new age of the customer");
                    String age = sc.nextLine();
                    System.out.println("Please enter the client's new place of residence");
                    String address = sc.nextLine();
                    // Creating Customer Objects
                    Customer s = new Customer();
                    s.setcid(cid);
                    s.setName(name);
                    s.setAge(age);
                    s.setAddress(address);
                    array.set(j, s);
                    // Gives an indication of successful modification
                    System.out.println("Modify Customer Success");
                    break;
                }
            }
        }
    }
}
