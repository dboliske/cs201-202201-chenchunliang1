package labs.lab7;

import java.util.Arrays;

/**
 * @author Chunliang Chen
 * @version 04-04-2022_04:40:46
 * @class CS201
 */
public class BubbleSort {
    public static void main (String[] args){
        int a[]={10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
        for (int i=0;i<a.length-1;i++){
            for (int j=0;j<a.length-1-i;j++){
                if (a[j] > a[j+1]) {
                    int temp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(a));//[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    }
}
