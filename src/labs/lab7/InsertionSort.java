package labs.lab7;

import java.util.Arrays;

/**
 * @author Chunliang Chen
 * @version 04-04-2022_04:48:42
 * @class CS201
 */
public class InsertionSort {
    public static   void main (String[] args){
        String a[]= {"cat", "fat", "dog", "apple", "bat", "egg"};
        for (int i=0;i<a.length;i++){
            String insertValue=a[i];
            int insertIndex=i-1;
            while(insertIndex>=0&&insertValue.compareTo(a[insertIndex])<0){
                a[insertIndex+1]=a[insertIndex];
                insertIndex--;
            }
            a[insertIndex+1]=insertValue;
        }
        System.out.println(Arrays.toString(a));//[apple, bat, cat, dog, egg, fat]
    }
}
