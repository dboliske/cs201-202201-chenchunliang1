package labs.lab7;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author Chunliang Chen
 * @version 04-04-2022_05:09:07
 * @class CS201
 */
public class BinarySearch {
    public static void main(String[] args) {
        String a[] = {"python", "ruby", "scala","c", "html", "java"};
        String [] b= arraySort(a);
        System.out.println("Sorted result of the array:");
        System.out.println(Arrays.toString(a));
        System.out.println("Please input of the string what you want to find:");
        Scanner sc = new Scanner(System.in);
        String aim = sc.nextLine();
        int index = binarySearch(b, aim, 0, b.length - 1);
        if (index>=0){
                System.out.println("The position of the array is " + index);
            }
         else System.out.println("No Such String");
    }

    private static String[] arraySort(String[] b) {
        for (int i=0;i<b.length-1;i++){
            for(int j=0;j<b.length-1-i;j++){
                if(b[j].compareTo(b[j+1])>0){
                    String temp=b[j];
                    b[j]=b[j+1];
                    b[j+1]=temp;
                }
            }
        }
        return b;
    }
    private static int binarySearch(String[] b, String aim, int left, int right) {
        if (left>right){
            return -1;
        }
        left = 0;
        right = b.length - 1;
        int mid;
        while(left<=right){
            mid=(left+right)/2;
            if(aim.compareTo(b[mid])<0){
                right=mid-1;
            }else if(aim.compareTo(b[mid])>0){
                left=mid+1;
            }else{
                return mid;
            }
        }
        return -1;
    }
}