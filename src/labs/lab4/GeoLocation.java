package labs.lab4;

import java.util.Objects;

/**
 * @author Chunliang Chen
 * @version 26-02-2022_17:04:40
 * @class CS201
 */
public class GeoLocation {
    private double lat;
    private double lng;

    public GeoLocation() {

    }

    public GeoLocation(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLat() {
        return this.lat;
    }

    public double getLng() {
        return this.lng;
    }

    @Override
    public String toString() {
        return "GeoLocation{" +
                "lat=" + this.lat +
                ", lng=" + this.lng +
                "}";
    }

    public void validLat(double lat) {
        if (lat >= 0 && lat <= 90) {
            lat = getLat();
        } else {
            System.out.println("lat is invalid");
        }
    }

    public void validLng(double lng) {
        if (lng >= 0 && lng <= 180) {
            lng = getLng();
        } else {
            System.out.println("lng is invalid");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        labs.lab4.GeoLocation that = (labs.lab4.GeoLocation) o;
        return Double.compare(that.lat, lat) == 0 && Double.compare(that.lng, lng) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(lat, lng);
    }
}
