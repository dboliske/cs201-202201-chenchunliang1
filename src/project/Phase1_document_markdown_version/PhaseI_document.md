## Phase I: Project Design



1. Describe the user interface. What are the menu options and how will the user use the application?



```java
public StoreManagement() {
	Scanner scanner = new Scanner(System.in);
	displayMenu();
	System.out.print("Please make a choice(Choose the number 1,2,3,4,5): ");
	String choice = scanner.nextLine().strip();
	while (!"5".equals(choice)) {
		if ("1".equals(choice)) {

		} else if ("2".equals(choice)) {

		} else if ("3".equals(choice)) {

		} else if ("4".equals(choice)) {

		}

		displayMenu();
		System.out.print("Please make a choice(Choose the number 1,2,3,4,5): ");
		choice = scanner.nextLine().strip();
	}
	System.out.println("Bye!");
}

public void displayMenu() {
	System.out.println("Here is the menu.");
	System.out.println("1) Create a new item");
	System.out.println("2) Sell items and remove them from the store");
	System.out.println("3) Search for an item");
	System.out.println("4) Modify items");
	System.out.println("5) Exit System");
}
```



2. Describe the programmers' tasks:

   - Describe how you will read the input file.
   
   
   
   use `java.io.BufferedReader` to read the file line by line util the end of the file
   
   
   
   - Describe how you will process the data from the input file.
   
   
   
   read the file line by line util the end of the file. split the line by  `java.lang.String#split(java.lang.String)`
   
   
   
   - Describe how you will store the data (what objects will you store?)
   
   
   
   In memory, I will use the `java.util.List`  and other Java Collections library to store the data. In disk, I use CSV(Comma-Separated Values) file to store the data.
   
   
   
   - How will you add/delete/modify data?
   - 
   
   I will use `List#add()` to add data. before deleting and modifying data, I will find the data to be deleted or to be updated.
   
   
   
   - How will you search the data?
   
   
   
   Basically, I will use `loop`  to traverse all the data and find the desired data. If the data is comparable, I will sort the data list, then use binary search algorithm to find the desired data, Thus the search process will be more quick.
   
   
   
   - List the classes you will need to implement your application.
   
   1. Main
   2. StoreManagement
   3. ProductItem
   4. AgeRestrictedItem
   5. Cart
   6. Utils
   
   
   
3. Draw a UML diagram that shows all classes in the program and their relationships. This can be done however you want. If you want a specific application, [StarUML](http://staruml.io/download) and [Draw.io](https://draw.io) are both good. But you are welcome to use any graphics program or just draw them by hand and scan them.



<img src="img/image-20220409004105837.png" alt="image-20220409004105837" style="zoom:50%;" />



4. Think how you will determine if your code functions are expected. Develop a test plan based on the above description; how will you test that the expected outputs have been achieved for every menu option? **Be sure this test plan is complete.** Your test plan should minimally test each option in the menu-driven user interface for expected behavior as well as error-handling. Your test plan should be in a spreadsheet format (preferably Excel, CSV, or TSV).

**NOTE**: You can use any format to write your design, but it must be in your repository in the project folder by the due date to be graded. This means it can **not** be a link to a Google Doc.

