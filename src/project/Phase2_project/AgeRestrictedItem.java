package project.Phase2_project;
/**
 * @author chunliang chen
 * @version  28-04-2022_14:11:36
 * @class CS201
 * AgeRestrictedItem is a subclass of ProductItem
 */
public class AgeRestrictedItem extends ProductItem {
    private Integer restrictedAge;

    public AgeRestrictedItem(String name, Double price, String expirationDate, Integer age) {
        super(name, price, expirationDate);
        this.restrictedAge = age;
    }

    public void setRestrictedAge(Integer restrictedAge) {
        this.restrictedAge = restrictedAge;
    }

    public Integer getRestrictedAge() {
        return restrictedAge;
    }

    @Override
    public String toString() {
        return "AgeRestrictedItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                (getExpirationDate() == null ? "" :
                        ", expirationDate='" + expirationDate + '\''
                ) +
                ", restrictedAge=" + restrictedAge +
                '}';
    }

    @Override
    public String savedString() {
        return name + "," + price + ',' + restrictedAge;
    }
}
