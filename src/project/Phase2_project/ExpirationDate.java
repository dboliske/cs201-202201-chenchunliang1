package project.Phase2_project;

/**
 * @author chunliang chen
 * @version  28-04-2022_10:05:27
 * @class CS201
 */
public class ExpirationDate {
    private String date;

    public ExpirationDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "ExpirationDate{" +
                "date='" + date + '\'' +
                '}';
    }
}
