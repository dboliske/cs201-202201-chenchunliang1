package project.Phase2_project;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author chunliang chen
 * @version 27-04-2022_01:05:33
 * @class CS201
 */
public class StoreManagement {

    private List<ProductItem> productItemList;
    private Cart cart;

    public StoreManagement() {
        productItemList = new ArrayList<>();
        cart = new Cart();
    }

    public void run() {
        loadDataFromFile();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            displayMenu();
            System.out.print("Please make a choice(Choose the number 1,2,3,4,5): ");
            String choice = scanner.nextLine();
            if ("1".equals(choice)) {
                addNewItem();
            } else if ("2".equals(choice)) {
                addToCart();
            } else if ("3".equals(choice)) {
                searchForProduct();
            } else if ("4".equals(choice)) {
                modifyProducts();
            } else if ("5".equals(choice)) {
                displayCart();
            } else if ("0".equals(choice)) {
                saveDataToFile();
                break;
            }
        }
        System.out.println("Bye!");
    }

    private void displayCart() {
        cart.displayCartInfo();
    }

    public void addToCart0() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Which product do you want to add to CART? Please input the id. ");
        System.out.print("ID? ");
        String idStr = scanner.nextLine();
        Integer id = Integer.parseInt(idStr);
        int index = -1;
        for (int i = 0; i < this.productItemList.size(); i++) {
            ProductItem productItem = productItemList.get(i);
            if (id.equals(productItem.getId())) {
                index = i;
                break;
            }
        }
        if (index != -1) {
            cart.addToCart(productItemList.get(index));
            productItemList.remove(index);
        }
    }

    private void addToCart() {
        printAllProducts();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Continue to add item to CART? (Yes or No)");
            String q = scanner.nextLine();
            if ("YES".equals(q) || "Yes".equals(q) || "yes".equals(q)) {
                addToCart0();
            } else if ("NO".equals(q) || "No".equals(q) || "no".equals(q)) {
                break;
            }
        }

    }

    private void modifyProducts() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("old name? ");
        String oldName = scanner.nextLine();
        System.out.print("new name? ");
        String newName = scanner.nextLine();
        System.out.print("new price? ");
        String newPriceStr = scanner.nextLine();
        double newPrice = Double.parseDouble(newPriceStr);
        for (ProductItem productItem : productItemList) {
            if (oldName.equals(productItem.getName())) {
                productItem.setName(newName);
                productItem.setPrice(newPrice);
            }
        }

    }

    private void searchForProduct() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("name? ");
        String name = scanner.nextLine();
        int cnt = 0;
        for (ProductItem productItem : productItemList) {
            if (name.equals(productItem.getName())) {
                System.out.println(productItem);
                cnt++;
            }
        }

        System.out.println("There are " + cnt + " products found!");
    }

    private void printAllProducts() {
        int total = productItemList.size();
        System.out.println("There are " + total + " products. ");
        for (ProductItem productItem : productItemList) {
            System.out.println(productItem);
        }
    }

    private void addNewItem() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("name? ");
        String name = scanner.nextLine();
        System.out.print("price? ");
        String priceStr = scanner.nextLine();
        Double price = Double.parseDouble(priceStr);
        String isExpiredStr = "";
        String expirationDate = "";
        while (true) {
            System.out.print("Expired? (Yes/No): ");
            isExpiredStr = scanner.nextLine();
            if ("YES".equals(isExpiredStr)
                    || "Yes".equals(isExpiredStr)
                    || "yes".equals(isExpiredStr)) {
                System.out.print("Expiration date? (MM/dd/yyyy): ");
                expirationDate = scanner.nextLine();
                break;
            } else if ("NO".equals(isExpiredStr)
                    || "No".equals(isExpiredStr)
                    || "no".equals(isExpiredStr)) {
                expirationDate = null;
                break;
            } else {
                System.out.println("Invalid Input!");
            }
        }
        String isAgeRestrictedStr = null;
        String ageStr = null;
        int age = 0;
        while (true) {
            System.out.print("Age Restricted? (Yes/No): ");
            isAgeRestrictedStr = scanner.nextLine();
            if ("YES".equals(isAgeRestrictedStr)
                    || "Yes".equals(isAgeRestrictedStr)
                    || "yes".equals(isAgeRestrictedStr)) {
                System.out.print("Restricted age? : ");
                ageStr = scanner.nextLine();
                age = Integer.parseInt(ageStr);
                break;
            } else if ("NO".equals(isAgeRestrictedStr)
                    || "No".equals(isAgeRestrictedStr)
                    || "no".equals(isAgeRestrictedStr)) {
                ageStr = null;
                break;
            } else {
                System.out.println("Invalid Input!");
            }
        }

        if (ageStr != null) {
            productItemList.add(
                    new AgeRestrictedItem(name, price, expirationDate, age)
            );
        } else {
            productItemList.add(
                    new ProductItem(name, price, expirationDate)
            );
        }
    }

    public void displayMenu() {
        System.out.println("==================================================");
        System.out.println("Here is the menu.");
        System.out.println("1) Create a new item");
        System.out.println("2) Sell items and remove them from the store");
        System.out.println("3) Search for an item");
        System.out.println("4) Modify items");
        System.out.println("5) Display Cart");
        System.out.println("0) Exit System");
        System.out.println("==================================================");
    }

    public void loadDataFromFile() {
        File file = null;
        BufferedReader reader = null;
        try {
            file = new File("src/project/Phase2_project/stock.csv");
            reader = new BufferedReader(new FileReader(file));
            String line = null;
            while ((line = reader.readLine()) != null) {
                String[] arr = line.split(",");
                if (arr.length == 2) {
                    productItemList.add(new ProductItem(arr[0], Double.parseDouble(arr[1]), null));
                } else if (arr.length == 3) {
                    if (arr[2].contains("/")) {
                        productItemList.add(new ProductItem(arr[0], Double.parseDouble(arr[1]), arr[2]));
                    } else {
                        productItemList.add(
                                new AgeRestrictedItem(arr[0],
                                        Double.parseDouble(arr[1])
                                        , null,
                                        Integer.parseInt(arr[2])
                                )
                        );
                    }
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("File Not Exists.");
        } catch (IOException e) {
            System.out.println("IO Exception");
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (Exception e) {

            }
        }
    }

    public void saveDataToFile() {
        File file = null;
        BufferedWriter writer = null;
        try {
            file = new File("src/project/Phase2_project/stock.csv");
            writer = new BufferedWriter(new FileWriter(file));
            for (ProductItem productItem : this.productItemList) {
                writer.write(productItem.savedString());
                writer.newLine();
            }
            writer.flush();
        } catch (FileNotFoundException e) {
            System.out.println("File Not Exists.");
        } catch (IOException e) {
            System.out.println("IO Exception");
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (Exception e) {

            }
        }
    }

    public List<ProductItem> getProductItemList() {
        return productItemList;
    }

    public void setProductItemList(List<ProductItem> productItemList) {
        this.productItemList = productItemList;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }
}
