package project.Phase2_project;

/**
 * @author chunliang chen
 * @version  29-04-2022_18:01:08
 * @class CS201
 */
public class Main {
    public static void main(String[] args) {
        new StoreManagement().run();
    }
}
