package project.Phase2_project;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author chunliang chen
 * @version 28-04-2022_01:24:08
 * @class CS201
 * ProductItem has an instance of ExpirationDate
 */
public class ProductItem {
    static AtomicInteger globalId = new AtomicInteger(1);

    protected Integer id;
    protected String name;
    protected Double price;
    protected ExpirationDate expirationDate;

    public ProductItem(String name, Double price, String expirationDate) {
        this.id = globalId.getAndIncrement();
        this.name = name;
        this.price = price;
        this.expirationDate = (expirationDate == null) ? null : new ExpirationDate(expirationDate);
    }

    public String savedString() {
        return name + "," + price + (expirationDate == null ? "" : "," + expirationDate.getDate());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public ExpirationDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(ExpirationDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public String toString() {
        return "ProductItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                (expirationDate == null ? "" :
                        (", expirationDate='" + expirationDate.getDate() + '\'')
                ) +
                '}';
    }
}
