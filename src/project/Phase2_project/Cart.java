package project.Phase2_project;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chunliang chen
 * @version 28-04-2022_16:29:47
 * @class CS201
 * cart
 */
public class Cart {
    private List<ProductItem> productItemList;

    public Cart() {
        productItemList = new ArrayList<>();
    }

    public void addToCart(ProductItem item) {
        this.productItemList.add(item);
    }

    public void displayCartInfo() {
        System.out.println("There are "+ productItemList.size() + " products in cart. ");
        for (ProductItem productItem : this.productItemList) {
            if (productItem instanceof AgeRestrictedItem) {
                AgeRestrictedItem item = (AgeRestrictedItem) productItem;
                System.out.println(item);
            } else {
                System.out.println(productItem);
            }
        }
    }
}
