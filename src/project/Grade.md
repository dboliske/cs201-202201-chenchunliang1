# Final Project

## Total

131/150

## Break Down

Phase 1:                                                            45/50

- Description of the user interface                                 3/3
- Description of the programmer's tasks:
  - Describe how you will read the input                            3/3
  - Describe how you will process the data from the input file      4/4
  - Describe how you will store the data                            3/3
  - How will you add/delete/modify data?                            5/5
  - How will you search data?                                       5/5
- Classes: List of names and descriptions                           7/7
- UML Class Diagrams                                                5/10
- Testing Plan                                                      10/10

Phase 2:                                                            86/100

- Compiles and runs with no run-time errors                         10/10
- Documentation                                                     10/15
- Test plan                                                         8/10
- Inheritance relationship                                          5/5
- Association relationship                                          5/5
- Searching works                                                   5/5
- Uses a list                                                       5/5
- Project reads data from a file                                    5/5
- Project writes data to a file                                     0/5
- Project adds, deletes, and modifies data stored in list           4/5
- Project generates paths between any two stations                  10/10
- Project encapsulates data                                         5/5
- Project is well coded with good design                            8/10
- All classes are complete (getters, setters, toString, equals...)  4/5

## Comments

### Design Comments

- UML diagram doesn't show relations. Make sure to show relations and implement produce, shelved, and age restricted items 
  to receive full points.  -5

### Code Comments

- Methods didn't have a brief description of its tasks. -5
- The test plan is incomplete.  -2
- Didn't implement the function of writing data to a file. -5
- The project didn't implement remove items from the cart. -1
- The Cart class didn't implement getters, setters. -1